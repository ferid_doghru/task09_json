package JNParser;

import ModelTask.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import java.io.File;

public class JSONParser {
    private ObjectMapper objectMapper;

   public JSONParser() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Candies> getCandies(File JsonFile) {
        List<Candies> candies = new ArrayList<>();
        try {
            candies = Arrays.asList(objectMapper.readValue(JsonFile, Candies[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return candies;
    }
}