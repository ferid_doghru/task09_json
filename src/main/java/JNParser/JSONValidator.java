package JNParser;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;
import java.io.File;
import java.io.IOException;


public class JSONValidator {
    public static boolean validation(File JsonFile , File schemaFile )
        throws IOException , ProcessingException {
        final JsonNode data = JsonLoader.fromFile(JsonFile);
        final JsonNode schema = JsonLoader.fromFile(schemaFile);
        final JsonSchemaFactory schemaFactory = JsonSchemaFactory.byDefault();
        JsonValidator jsonValidator = schemaFactory.getValidator();
        ProcessingReport processingReport = jsonValidator.validate(schema , data);
        return processingReport.isSuccess();
    }



}
