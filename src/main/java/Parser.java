import ModelTask.Candies;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import java.util.List;
import com.fasterxml.jackson.core.JsonParser;
import  JNParser.*;
import java.io.File;
import java.io.IOException;

public class Parser {
    public static void main(String [] args) {
        File Json = new File("src\\main\\resources\\candies.json");
        File schema = new File("src\\main\\resources\\schema.json");

        try {
            if (JSONValidator.validation(Json, schema)) {
                JSONParser parser = new JSONParser();
                List<Candies> candies = parser.getCandies(Json);
                candies.stream().sorted(new CandiesComparator()).map(Candies::toString).forEach(System.out::println);
            } else {
                System.out.println("Not valid!");
            }
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
        }
    }
}
