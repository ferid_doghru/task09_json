package ModelTask;

public class Candies {
    private String name ;
    private int energy;
    private String type;
    private Ingredients ingredients;
    private String value;
    private String production;

    Candies(String name , int energy , String type , Ingredients ingredients , String value , String production ){
        this.setName(name);
        this.setEnergy(energy);
        this.setType(type);
        this.setIngredients(ingredients);
        this.setValue(value);
        this.setProduction(production);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Ingredients getIngredients() {
        return ingredients;
    }

    public void setIngredients(Ingredients ingredients) {
        this.ingredients = ingredients;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }
}
