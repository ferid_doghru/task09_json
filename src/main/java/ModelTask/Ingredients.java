package ModelTask;

public class Ingredients {
    private int water;
    private int sugar;
    private int fructose;
    private String chocolateType;
    private int vanilin;
    Ingredients(){
    }

    public int getWater() {
        return water;
    }

    public void setWater(int water) {
        this.water = water;
    }

    public int getSugar() {
        return sugar;
    }

    public void setSugar(int sugar) {
        this.sugar = sugar;
    }

    public int getFructose() {
        return fructose;
    }

    public void setFructose(int fructose) {
        this.fructose = fructose;
    }

    public String getChocolateType() {
        return chocolateType;
    }

    public void setChocolateType(String chocolateType) {
        this.chocolateType = chocolateType;
    }

    public int getVanilin() {
        return vanilin;
    }

    public void setVanilin(int vanilin) {
        this.vanilin = vanilin;
    }
}

